/* Third party */

//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/bootstrap/dist/js/bootstrap.min.js
//= ../../bower_components/slick-carousel/slick/slick.min.js
//= ../../bower_components/skrollr/dist/skrollr.min.js
//= ../../bower_components/superslides/dist/jquery.superslides.min.js
//= ../../bower_components/owl.carousel/dist/owl.carousel.min.js

/* Custom */

//= partials/app.js
//= partials/slider.js
//= partials/scroller.js
//= partials/team-slider.js
//= partials/animation.js
//= partials/we-do-slider.js
//= partials/map.js