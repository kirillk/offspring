var infowindow = null;
$(document).ready(function () {
  initialize();
});

function initialize() {

  var centerMap = new google.maps.LatLng(-37.8236033, 145.03531605);

  var myOptions = {
    zoom: 19,
    center: centerMap,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  var map = new google.maps.Map(document.getElementById("map"), myOptions);

  setMarkers(map, sites);
  infowindow = new google.maps.InfoWindow({
    content: "loading..."
  });
}

var sites = [
	['Offspring', -37.8236033, 145.03531605, 4, 'Offspring - Child Health Specialists'],
	/*['Tram Stop 73', -37.82274523, 145.03526106, 2, 'This is the tram stop 73.'],
	['Council parking', -37.82393593, 145.03637686, 1, 'This is Council parking.'],
	['Parking', -37.822009, 145.036654, 3, 'This is parking.']*/
];

function setMarkers(map, markers) {

  for (var i = 0; i < markers.length; i++) {
    var sites = markers[i];
    var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);
    var marker = new google.maps.Marker({
      position: siteLatLng,
      map: map,
      title: sites[0],
      zIndex: sites[3],
      html: sites[4]
    });

    google.maps.event.addListener(marker, "click", function () {
      infowindow.setContent(this.html);
      infowindow.open(map, this);
    });
  }
}