;(function($) {
  
  $('.scroll').on('click', function(e) {
		e.preventDefault();
	 
		var href = $(this).attr('href');
		var target = $(href);			
		var top = target.offset().top;
		var height = $('#sticker').outerHeight(true);
    
    if($(window).innerWidth() >= 881) {
      height = $('#sticker').height();
      
      $('html, body').animate({
        scrollTop: top - height
      }, 1200);  
    } else {
      $('html, body').animate({
        scrollTop: top
      }, 1200);  
    };
	});
    
  $('.b-logo').on('click', function() {
		$('body,html').animate({
			scrollTop: 0
		}, 1200);
		return false;
	});
  
  $(window).one('scroll', function() {
    $('.b-scroller').addClass('active');
  });
  
})(jQuery);
