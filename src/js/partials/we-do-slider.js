;(function($) {
	var owl = $('#work-slider');
	
  owl.owlCarousel({
		items: 1,
		loop:false,
		URLhashListener: true,
		autoplayHoverPause:true,
		startPosition: 'URLHash'
	});
	
	owl.on('changed.owl.carousel', function(event) {
		var index = event.item.index;
		
		return (function() {
			if(index == 0) {
				$('#speciality').addClass('active');
				$('#clinics').removeClass('active');
			}
			
			if(index == 1) {
				$('#speciality').removeClass('active');
				$('#clinics').addClass('active');
			}
		})(index);
	});
})(jQuery);