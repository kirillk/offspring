;(function ($) {
  /*$('#team-slider').slick({
    slidesToShow: 4,
    infinite: true,
    nextArrow: $('.b-next'),
    prevArrow: $('.b-prev'),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });*/

  var owl = $('#team-slider');
  owl.owlCarousel({
    loop: true,
    autoHeight:true,
    items: 1
    /*responsive: {
      1200: {
        items: 4
      },
      1024: {
        items: 3
      },
      768: {
        items: 2
      },
      480: {
        items: 1
      },
      0: {
        items: 1
      }
    }*/
  })

  $('.b-next').on('click', function (e) {
    e.preventDefault();
    owl.trigger('next.owl.carousel');
  });
  $('.b-prev').on('click', function (e) {
    e.preventDefault();
    owl.trigger('prev.owl.carousel');
  });
})(jQuery);